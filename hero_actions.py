from datetime import timedelta

from hero_action import HeroAction, ConditionFail
from hero_info import HeroInfo
from hero_utils import now


class Alert(HeroAction):
    def _call(self):
        self.alert = True


class Resurrect(HeroAction):
    ON = ['health']

    def _call(self):
        if self.info.hero.health == 0:
            self.hero.resurrect()
        else:
            raise ConditionFail('Not dead')


class Punish(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.punish_needs_godpower}

    def _call(self):
        self.hero.punish()


class Encourage(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.encourage_needs_godpower}

    def _call(self):
        self.hero.encourage()


class Miracle(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.miracle_needs_godpower}

    def _call(self):
        self.hero.miracle()


class AccRestore(HeroAction):
    CONDITIONS = {'accumulator_min': 1}

    def _call(self):
        self.hero.acc_restore()


class AccAccumulate(HeroAction):
    CONDITIONS = {'godpower_min': 100}

    def _call(self):
        self.hero.acc_restore()


class Voice(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.voice_needs_godpower}

    def __init__(self, hero, voice, **kwargs):
        if isinstance(voice, str) and voice in hero.voices:
            voice = hero.voices[voice]
        if isinstance(voice, dict) and 'voices' in voice:
            conditions = voice.copy()
            conditions.pop('voices')
            for k, v in conditions.items():
                kwargs.setdefault(k, v)
        super().__init__(hero, voice=voice, **kwargs)

    def _call(self, voice):
        if (not self.info.hero.arena_fight and self.hero._voice_next_time and
           self.hero._voice_next_time > now()):
            raise ConditionFail('next time in future: %s' % self.hero._voice_next_time)
        self.hero.voice(voice)

        for _ in range(3):
            info = HeroInfo(self.hero.get_info())
            infl, resp = self._find_diary_voice_influence(info)
            if infl:
                break
            self.hero.sleep(2, reason='Waiting influence in diary')
        if not infl:
            raise RuntimeError('Voice sent, influence not found')

        if resp:
            delta = timedelta(seconds=self.hero.params.voice_response_timeout)
        else:
            delta = timedelta(seconds=self.hero.params.voice_no_response_timeout)
        self.hero._voice_next_time = (now() > infl.time and now() or infl.time) + delta
        # self.hero._info_update = info
        return 'Voice influence %s: %s: %s' % (infl.time, infl.msg, resp and resp.msg or '-')

    def _find_diary_voice_influence(self, info):
        last_diary = [log for log in self.info.diary if 'vote_id' not in log][0]
        infl, resp = None, None

        for i, log in enumerate(info.diary):
            if log.time == last_diary.time and log.msg == last_diary.msg:
                # We can't rely on time because new influences may be
                # actually older, but appears after last diary
                break
            if log.get('infl'):
                assert not infl, '"infl" found more than one: {}'.format(log)
                infl = log
                if i and (info.diary[i-1].time - log.time).total_seconds() == 10:
                    resp = info.diary[i-1]
        return infl, resp


class Heal(HeroAction):
    ON = ['health', 'godpower', 'fight_type']

    # NOTE: by default not working on arena, because it may be zpg!
    # Also not working by default in dungeon corridors because it may be not effective
    CONDITIONS = {'activity_type_in': ['FIGHT'],
                  'fight_type_in': ['monster', 'monster_m', 'multi_monster']}

    def __init__(self, hero, voice=False, encourage=False, **kwargs):
        if not voice and not encourage:
            raise ValueError('voice or encourage must be supplied')
        if not voice:
            kwargs.setdefault('godpower_min', hero.constants.encourage_needs_godpower)
        else:
            kwargs.setdefault('godpower_min', hero.constants.voice_needs_godpower)
        super().__init__(hero, voice=voice, encourage=encourage, **kwargs)

    def _call(self, voice=False, encourage=False, check_abilities=True):
        if 'opponent' in self.info and 'hero' in self.info.opponent:
            ab = [ab_.lower() for ab_ in self.info.opponent.hero.get('ab', [])]
        else:
            ab = []

        if encourage and self.info.hero.godpower >= self.hero.constants.encourage_needs_godpower:
            if check_abilities and voice and 'паразитирующий' in ab and 'глушащий' not in ab:
                self.logger.debug('Skipping encourage because of opponent abilities: %s', ab)
            else:
                self.hero.encourage()
                return 'encouraged'
        if voice:
            if check_abilities and 'глушащий' in ab:
                self.logger.debug('Skipping voice because of opponent abilities: %s', ab)
            else:
                self.hero.voice(self.hero.voices.heal)
                return 'voice sent'


class DiaryVoiceVote(HeroAction):
    ON = ['diary']
    CONDITIONS = {'godpower_max': 99}

    def __init__(self, *args, **kwargs):
        self._voted = []
        super().__init__(*args, **kwargs)

    def _call(self):
        voted = []
        for log in self.info.diary:
            if log.get('voice') and log.get('voice_id'):
                if not log['voice_id'] in self._voted:
                    self.hero.diary_voice_vote(log['voice_id'], True)
                    voted.append(log['voice_id'])
        if voted:
            self._voted.extend(voted)
            self._voted = self._voted[-10:]
            return 'Voted: %s' % voted
        raise ConditionFail('Nothing to vote')


class InventoryActivate(HeroAction):
    ON = ['inventory', 'gp']
    CONDITIONS = {'activity_type_in': ['FIELD', 'TOWN']}

    def _call(self, names=[], groups=[]):
        activated = []
        for name, info in self.info.inventory_active.items():
            if name in names:
                if self._activate(name, info):
                    activated.append(name)
            else:
                for group in groups:
                    if name in self.hero.inventory_groups[group]:
                        if self._activate(name, info):
                            activated.append(name)
        if activated:
            return 'Activated: %s' % activated
        raise ConditionFail('Nothing to activate')

    def _activate(self, name, info):
        if self.info.hero.godpower >= info.get('needs_godpower', 0):
            self.hero.inventory_activate(name)
            return True
        else:
            self.logger.warning('Not enough godpower for %s', name)
            return False


class SendToZpgArena(HeroAction):
    CONDITIONS = {
        'godpower_min': lambda h: h.constants.send_to_arena_needs_godpower,
        'level_min': 10,
    }

    def _call(self):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.is_arena_available and
            not self.info.hero.arena_send_after
        ):
            raise ConditionFail('Arena not available')

        if self.info.ctime.minute not in self.hero.constants.zpg_arena_minutes:
            raise ConditionFail('Arena is not ZPG')

        if not self.hero.send_to_zpg_arena():
            raise ConditionFail('Arena is not ZPG (send failed)')

        # Looks like "ареналин" doesn't work with ZPG
        # if 'ареналин' in self.info.inventory:
        #     self.hero.inventory_activate('ареналин')
        # else:
        #     self.hero.send_to_zpg_arena()


class SendToDungeon(HeroAction):
    CONDITIONS = {'godpower_min': lambda h: h.constants.send_to_dungeon_needs_godpower}

    def __init__(self, *args, **kwargs):
        self._wood_avail = None
        super().__init__(*args, **kwargs)

    def _call(self, check_wood=True):
        if not (
            not self.info.hero.arena_fight and
            self.info.hero.d_a and
            not self.info.hero.d_send_after
        ):
            raise ConditionFail('Dungeon not available')

        if check_wood:
            if not self._wood_avail:
                self._set_wood_avail()
            if self._wood_avail is not True and self._wood_avail > now():
                raise ConditionFail('Wood available in future: {}'.format(self._wood_avail))
            # check last time RIGHT before sending
            self._set_wood_avail()
            if self._wood_avail is not True:
                raise ConditionFail('Wood not available ({})'.format(self._wood_avail))

        self.hero.send_to_dungeon()

    def _set_wood_avail(self):
        self._wood_avail = self.hero.check_dungeon_wood()
        if self._wood_avail is not True:
            self._wood_avail = now() + self._wood_avail
