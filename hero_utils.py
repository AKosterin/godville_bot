import os
import sys
import json
import collections
import logging
from shutil import which
from datetime import datetime, date, timedelta

import yaml
from dateutil.tz import tzlocal
from playsound import playsound as playsound_


def load_settings(*filenames, settings_path='hero_settings.yaml', hero_path='hero.yaml'):
    settings_path = os.path.join(os.path.dirname(__file__), settings_path)
    hero_path = os.path.join(os.path.dirname(__file__), hero_path)

    settings = yaml.load(open(settings_path, 'rb').read())
    if os.path.exists(hero_path):
        dict_merge(settings, yaml.load(open(hero_path, 'rb').read()))
    for filename in filenames:
        dict_merge(settings, yaml.load(open(filename, 'rb').read()))

    return maybe_attr_dict(settings)


def configure_logging(settings):
    import http.client
    if settings.logging.http_debug:
        http.client.HTTPConnection.debuglevel = 1
    else:
        http.client.HTTPConnection.debuglevel = 0

    import coloredlogs
    field_styles = coloredlogs.DEFAULT_FIELD_STYLES.copy()
    field_styles.update(settings.logging.field_styles)
    level_styles = coloredlogs.DEFAULT_LEVEL_STYLES.copy()
    level_styles.update(settings.logging.level_styles)
    coloredlogs.install(
        field_styles=field_styles, level_styles=level_styles,
        **{k: v for k, v in settings.logging.items() if k in ['level', 'fmt', 'datefmt']}
    )
    for name, level in settings.logging.levels.items():
        logger = logging.getLogger(name)
        logger.setLevel(level)

    # Turn off pyparso logging in ipython
    logger = logging.getLogger('parso')
    if logger:
        logger.propagate = False


def run_ipython(context, *autorun, pdb=False):
    import IPython
    import IPython.lib.pretty
    from traitlets.config.loader import Config

    IPython.lib.pretty.for_type(dict, lambda obj, *args: pprint(obj))
    IPython.lib.pretty.for_type(list, lambda obj, *args: pprint(obj))
    IPython.lib.pretty.for_type(tuple, lambda obj, *args: pprint(obj))

    config = Config({
        'InteractiveShellApp': {
            'exec_lines': [
                '%load_ext autoreload',
                '%autoreload 2',
            ] + (['%pdb'] if pdb else []) + list(autorun),
        },
        'TerminalInteractiveShell': {
            'banner1': ('Python %s on %s; IPython %s\n' % (
                        sys.version.split('\n')[0].strip(), sys.platform,
                        IPython.__version__)),
         },
    })
    IPython.start_ipython([], user_ns=context, config=config)


def pprint(obj, indent=2, colors=True):
    def default(obj):
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        elif isinstance(obj, timedelta):
            return str(obj)
        else:
            return repr(obj)
    rv = json.dumps(obj, default=default, indent=indent, ensure_ascii=False)

    if colors:
        try:
            from pygments import highlight
            from pygments.lexers import JsonLexer
            from pygments.formatters import TerminalFormatter
        except ImportError:
            pass
        else:
            rv = highlight(rv, JsonLexer(), TerminalFormatter())

    print(rv.strip(), end='')


def get_action_registry(action_modules):
    from hero_action import HeroAction
    registry = {}
    for module_name in action_modules:
        for action_cls in import_module_objects(module_name, base_cls=HeroAction):
            if action_cls.name in registry:
                raise ValueError('Action already exists: %s' % action_cls.name)
            registry[action_cls.name] = action_cls
    return registry


def now(ms=True):
    rv = datetime.now(tzlocal())
    if not ms:
        return rv.replace(microsecond=0)
    return rv


class AttrDict(dict):
    __getattr__ = dict.__getitem__

    def __init__(self, data):
        super().__init__({k: maybe_attr_dict(v) for k, v in data.items()})

    def __dir__(self):
        # autocompletion for ipython
        return super().__dir__() + list(self.keys())


def maybe_attr_dict(data, rec=True, rec_seq=True):
    if isinstance(data, dict):
        if rec:
            return AttrDict({k: maybe_attr_dict(v, rec_seq=rec_seq)
                             for k, v in data.items()})
        return AttrDict(data)
    elif rec_seq and isinstance(data, (list, tuple)):
        return data.__class__(maybe_attr_dict(v, rec=rec) for v in data)
    else:
        return data


def playsound(path, shell=None):
    if not shell:
        if which('cvlc'):
            shell = 'cvlc --play-and-exit'
        elif which('mpg123') and path.endswith('.mp3'):
            # For my system it's proper working with:
            # shell = "mpg123 -a hw:0,0"
            shell = 'mpg123'
        elif which('mpg321') and path.endswith('.mp3'):
            shell = 'mpg321'
    if shell:
        return os.popen('{} "{}"'.format(shell, path))
    return playsound_(path)


class EntityLoggerAdapter(logging.LoggerAdapter):
    def __init__(self, logger, entity):
        self.logger = logger
        self.entity = entity or '?'

    def process(self, msg, kwargs):
        return '{}: {}'.format(self.entity, msg), kwargs


def get_changed_keys(d1, d2):
    # https://stackoverflow.com/a/18860653/450103
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = set(k for k in intersect_keys if d1[k] != d2[k])
    return added | removed | modified


class classproperty(property):
    """
    A decorator that behaves like @property except that operates
    on classes rather than instances.
    Copy of sqlalchemy.util.langhelpers.classproperty, because first one executed
    on class declaration.
    """

    def __init__(self, fget, *arg, **kw):
        super(classproperty, self).__init__(fget, *arg, **kw)
        self.__doc__ = fget.__doc__

    def __get__(desc, self, cls):
        return desc.fget(cls)


def capital_to_underscore(name):
    res = name[0].lower()
    for char in name[1:]:
        if char.isupper():
            res += '_' + char.lower()
        else:
            res += char
    return res


def import_module_objects(module_name, base_cls):
    rv = []
    module = __import__(module_name)
    for attr in dir(module):
        obj = getattr(module, attr)
        try:
            if issubclass(obj, base_cls) and obj is not base_cls:
                rv.append(obj)
        except TypeError:
            pass
    return rv


def dict_merge(d, u):
    # https://stackoverflow.com/a/3233356/450103
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            d[k] = dict_merge(d.get(k, {}), v)
        else:
            d[k] = u[k]
    return d
