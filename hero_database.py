import pickle

from peewee import (Model, PrimaryKeyField, DateTimeField, IntegerField,
                    TextField, BooleanField, BlobField)
from playhouse.sqlite_ext import SqliteExtDatabase


def create_database(filename):
    db = SqliteExtDatabase(filename)

    class BaseModel(Model):
        class Meta:
            database = db

    class State(BaseModel):
        id = PrimaryKeyField()
        cookies = BlobField(null=True)

    class DiaryLog(BaseModel):
        id = PrimaryKeyField()
        time = DateTimeField()
        msg = TextField()
        infl = BooleanField(null=True)

        health = IntegerField()
        godpower = IntegerField()
        gold = IntegerField()
        bricks_cnt = IntegerField()
        wood_cnt = IntegerField(null=True)
        accumulator = IntegerField()
        inventory_num = IntegerField()

        quest = TextField()
        quest_progress = IntegerField()
        quests_completed = IntegerField()

        level = IntegerField()
        exp_progress = IntegerField()
        alignment = TextField()

        aura_name = TextField(null=True)
        distance = IntegerField()
        town_name = TextField(null=True)
        retirement = TextField(null=True)

    db.connect()
    db.create_tables([State, DiaryLog], safe=True)

    return State, DiaryLog


class HeroDatabase(object):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        db_state, self.db_diary_log = \
            create_database(self.username + '.db')
        self.db_state = db_state.get_or_create(id=0)[0]

    def update_diary_log(self):
        last = tuple(self.db_diary_log.select().order_by(-self.db_diary_log.id).limit(1))
        last = last and last[0] or None
        write = []
        for log in self.info.diary:
            if last and log.msg == last.msg and str(log.time) == last.time:
                break
            write.append(log)
        for log in reversed(write):
            if 'vote_id' not in log:  # voices for voting may disappear
                self._append_diary_log(log)

    def _append_diary_log(self, log):
        kwargs = {}
        fields = self.db_diary_log._meta.fields
        for field in fields:
            if field in log:
                kwargs[field] = log[field]
            elif field in self.info.hero:
                kwargs[field] = self.info.hero[field]
            elif fields[field].null:
                kwargs[field] = None
            else:
                assert field in ['id'], '{} may not be null'.format(field)
        self.db_diary_log.create(**kwargs)

    def get_state(self, name, serialized=False):
        assert name in self.db_state._meta.fields
        value = getattr(self.db_state, name)
        return (serialized and value) and pickle.loads(value) or value

    def set_state(self, name, value, serialized=False):
        assert name in self.db_state._meta.fields
        setattr(self.db_state, name, serialized and pickle.dumps(value) or value)
        self.db_state.save()
