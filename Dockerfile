FROM python:3.7.0-alpine3.8
ADD . /godvile_bot/
WORKDIR /godvile_bot/
RUN apk add --update \
  build-base \
  libxml2-dev \
  libxslt-dev \
  sqlite-dev \
  && rm -rf /var/cache/apk/*
 
RUN pip install -r ./requirements.txt
CMD python ./hero.py /godvile_bot/config/hero.yaml --run