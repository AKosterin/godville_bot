import re
import inspect

from hero_utils import (EntityLoggerAdapter, classproperty, capital_to_underscore)


class ConditionFail(Exception):
    pass


class Condition:
    STR_ATTRS = (
        'activity_type', 'fight_type', 'town_name', 'c_town', 'aura_name',
        'monster_name', 'alignment', 'aln', 'quest'
    )
    NUM_ATTRS = (
        'gold', 'godpower', 'health', 'quest_progress', 'quests_completed', 'distance',
        'accumulator', 'exp_progress', 'inventory_num', 'level', 'health',
        'health_percent', 'inventory_percent', 'inventory_active_cnt',
        'inventory_bold_cnt', 'inventory_heal_cnt', 'inventory_common_cnt',
        'opps_health', 'opps_health_percent', 'alls_health',
        'alls_health_percent', 'opponent_health_percent',
        'alls_alive', 'opps_alive', 'pet_level'
    )
    BOOL_ATTRS = ('pet_is_dead',)

    # implement key, attrs, condition_types
    key, attrs, condition_types = None, None, None
    fail_on_none = False

    def __init__(self, attr, condition):
        self.attr = attr
        self.name = attr + '_' + self.key
        if not callable(condition) and not isinstance(condition, tuple(self.condition_types)):
                raise TypeError('%s condition is not in types %s' %
                                (self.name, self.condition_types))
        self.condition = condition

    def __call__(self, *args, **kwargs):
        return self.check(*args, **kwargs)

    def check(self, hero):
        try:
            value = getattr(hero.info, self.attr)
        except KeyError:
            try:
                value = getattr(hero.info.hero, self.attr)
            except KeyError:
                value = None

        if value is None and self.fail_on_none:
            raise ConditionFail('%s is None' % self.name)

        condition = self.condition
        if callable(condition):
            condition = condition(hero)
            if not isinstance(condition, tuple(self.condition_types)):
                    raise TypeError('%s condition is not in types %s' %
                                    (self.name, self.condition_types))

        error = self._check(value, condition)
        if error:
            raise ConditionFail(error)

    def _check(self, value, condition):
        raise NotImplementedError()


class InCondition(Condition):
    key = 'in'
    attrs = Condition.STR_ATTRS
    condition_types = [tuple, list]

    def _check(self, value, condition):
        if value not in condition:
            return '%s: %s not in %s' % (self.name, value, condition)


class NotInCondition(Condition):
    key = 'notin'
    attrs = Condition.STR_ATTRS
    condition_types = [tuple, list]

    def _check(self, value, condition):
        if value in condition:
            return '%s: %s in %s' % (self.name, value, condition)


class ContainsCondition(Condition):
    key = 'contains'
    attrs = Condition.STR_ATTRS
    condition_types = [str]
    fail_on_none = True

    def _check(self, value, condition):
        if condition not in value:
            return '%s: %s not contains %s' % (self.name, value, condition)


class NotContainsCondition(Condition):
    key = 'notcontains'
    attrs = Condition.STR_ATTRS
    condition_types = [str]

    def _check(self, value, condition):
        if value is not None and condition in value:
            return '%s: %s contains %s' % (self.name, value, condition)


class RegexpCondition(Condition):
    key = 'regexp'
    attrs = Condition.STR_ATTRS
    condition_types = [str]
    fail_on_none = True

    def _check(self, value, condition):
        if not re.search(condition, value):
            return '%s: %s not matched regexp %s' % (self.name, value, condition)


class NotRegexpCondition(Condition):
    key = 'notregexp'
    attrs = Condition.STR_ATTRS
    condition_types = [str]

    def _check(self, value, condition):
        if value is not None and re.search(condition, value):
            return '%s: %s matched regexp %s' % (self.name, value, condition)


class EqCondition(Condition):
    key = 'eq'
    attrs = Condition.STR_ATTRS + Condition.NUM_ATTRS + Condition.BOOL_ATTRS
    condition_types = [str, int]

    def _check(self, value, condition):
        if condition != value:
            return '%s: %s != %s' % (self.name, value, condition)


class NotEqCondition(Condition):
    key = 'noteq'
    attrs = Condition.STR_ATTRS + Condition.NUM_ATTRS + Condition.BOOL_ATTRS
    condition_types = [str, int]

    def _check(self, value, condition):
        if condition == value:
            return '%s: %s == %s' % (self.name, value, condition)


class MaxCondition(Condition):
    key = 'max'
    attrs = Condition.NUM_ATTRS
    condition_types = [int, float]
    fail_on_none = True

    def _check(self, value, condition):
        if value > condition:
            return '%s: %s > %s' % (self.name, value, condition)


class MinCondition(Condition):
    key = 'min'
    attrs = Condition.NUM_ATTRS
    condition_types = [int, float]
    fail_on_none = True

    def _check(self, value, condition):
        if value < condition:
            return '%s: %s < %s' % (self.name, value, condition)


class GteCondition(MinCondition):
    key = 'gte'


class GtCondition(Condition):
    key = 'gt'
    attrs = Condition.NUM_ATTRS
    condition_types = [int, float]
    fail_on_none = True

    def _check(self, value, condition):
        if value <= condition:
            return '%s: %s <= %s' % (self.name, value, condition)


class LteCondition(MaxCondition):
    key = 'lte'


class LtCondition(Condition):
    key = 'lt'
    attrs = Condition.NUM_ATTRS
    condition_types = [int, float]
    fail_on_none = True

    def _check(self, value, condition):
        if value >= condition:
            return '%s: %s >= %s' % (self.name, value, condition)


conditions_registry = [
    InCondition, NotInCondition, ContainsCondition, NotContainsCondition,
    RegexpCondition, NotRegexpCondition, EqCondition, NotEqCondition,
    MaxCondition, MinCondition, GteCondition, GtCondition, LteCondition, LtCondition
]
conditions_registry = {c.key: c for c in conditions_registry}


class HeroAction:
    ON = ['update']
    CONDITIONS = {}

    def __init__(self, hero, alert=False, alert_path=None, alert_shell=None,
                 on=None, debug=None, stop_propagation=False, **kwargs):
        self.hero = hero
        self.logger = EntityLoggerAdapter(hero.logger, self.name)

        self.alert, self.alert_path, self.alert_shell = alert, alert_path, alert_shell
        self.on = (on is not None) and on or self.ON
        assert isinstance(self.on, (tuple, list)), '"on" argument should be tuple or list'
        self.debug = (debug is None) and hero.params.debug or debug
        self.stop_propagation = stop_propagation

        self.params = self.PARAMS.copy()
        for k, v in self.CONDITIONS.items():
            kwargs.setdefault(k, v)
        self.conditions = []
        for k, v in kwargs.items():
            if k in self.params:
                self.params[k] = v
                continue

            *attr, key = k.split('_')
            attr = '_'.join(attr)
            try:
                cls = conditions_registry[key]
                assert attr in cls.attrs
            except (KeyError, AssertionError):
                raise TypeError('{} got unexpected parameter "{}"'.format(self.name, k))
            self.conditions.append(cls(attr, v))

        for k in self.params:
            if self.params[k] == inspect._empty:
                raise TypeError('{} require parameter "{}"'.format(self.name, k))

    @classproperty
    def PARAMS(cls):
        sig = inspect.signature(cls._call)
        return {k: v.default for k, v in tuple(sig.parameters.items())[1:]}

    @classproperty
    def name(cls):
        if not hasattr(cls, '_name'):
            cls._name = capital_to_underscore(cls.__name__)
        return cls._name

    @property
    def info(self):
        return self.hero.info

    def __call__(self):
        try:
            for condition in self.conditions:
                condition.check(self.hero)
            res = self._call(**self.params)
        # except ConditionFail as exc:
        #     if self.debug:
        #         self.logger.debug('Condition fail: %s', exc)
        #     return False
        except Exception as exc:
            # This is dirty trick for ipython autoreload,
            # because of dynamic importing actions
            # (to allow custom action modules from yaml configuration)
            # there is different ConditionFail objects in the end
            # TODO: maybe fix it in some other way?
            if exc.__class__.__name__ == 'ConditionFail':
                if self.debug:
                    self.logger.debug('Condition fail: %s', exc)
                return
            raise
        else:
            self.logger.info(res or 'Done')
            if self.alert:
                self.hero.alert(self.alert_path, self.alert_shell)
            if self.stop_propagation:
                return True

    def _call(self):
        raise NotImplementedError()
