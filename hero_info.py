from dateutil.parser import parse as dateutil_parse

from hero_utils import AttrDict


class HeroInfo(AttrDict):
    def __init__(self, data):
        super().__init__(data)

        assert self.hero.max_gp == 100

        def _parse_item_time(item, time_key='time'):
            item[time_key] = dateutil_parse(item[time_key])
            return item

        _parse_item_time(self, 'ctime')
        if 'news_from_field' in self:
            _parse_item_time(self.news_from_field)
        if 'arena_fight_log' in self:
            self['arena_fight_log'] = [_parse_item_time(log)
                                       for log in self['arena_fight_log']]
        self['diary'] = [_parse_item_time(log)
                         for log in self.get('diary', [])]
        self['imp_e'] = [_parse_item_time(log)
                         for log in self.get('imp_e', [])]

        for attr in dir(self.__class__):
            if (not attr.startswith('_') and
               attr not in self.keys() and
               isinstance(getattr(self.__class__, attr), property)):
                self[attr] = getattr(self, attr)

    @property
    def health_percent(self):
        return (self.hero.health / self.hero.max_health) * 100

    @property
    def inventory_percent(self):
        return (self.hero.inventory_num / self.hero.inventory_max_num) * 100

    @property
    def inventory_active(self):
        return{k: v for k, v in self.inventory.items()
               if v.get('activate_by_user')}

    @property
    def inventory_active_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.get('activate_by_user'))

    @property
    def inventory_bold_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.price and not i.get('activate_by_user'))

    @property
    def inventory_heal_cnt(self):
        return sum(i.cnt for i in self.inventory.values()
                   if i.get('type') == 'heal_potion')

    @property
    def inventory_common_cnt(self):
        return (self.hero.inventory_num - self.inventory_active_cnt -
                self.inventory_bold_cnt - self.inventory_heal_cnt)

    @property
    def opponent_health_percent(self):
        if 'opponent' in self and 'health' in self.opponent.hero:
            return (self.opponent.hero.health / self.opponent.hero.max_health) * 100

    @property
    def opps_health(self):
        if self.get('opps'):
            return sum(h.get('health', h.get('hp')) for h in self.opps)

    @property
    def opps_max_health(self):
        if self.get('opps'):
            return sum(h.get('max_health', h.get('hpm')) for h in self.opps)

    @property
    def opps_health_percent(self):
        if self.get('opps'):
            return (self.opps_health / self.opps_max_health) * 100

    @property
    def opps_alive(self):
        min_hp = self.hero.get('fight_type') == 'town' and 1 or 0
        return sum(1 for h in self.get('opps', [])
                   if h.get('health', h.get('hp')) > min_hp)

    @property
    def alls_health(self):
        return sum(h.hp for h in self.get('alls', [])) + self.hero.health

    @property
    def alls_max_health(self):
        return sum(h.hpm for h in self.get('alls', [])) + self.hero.max_health

    @property
    def alls_health_percent(self):
        return (self.alls_health / self.alls_max_health) * 100

    @property
    def alls_alive(self):
        return (sum(1 for h in self.get('alls', []) if h.hp > 1) +
                (self.hero.health > 1 and 1 or 0))

    @property
    def activity_type(self):
        if self.hero.arena_fight:
            return 'FIGHT'
        if self.hero.get('town_name'):
            return 'TOWN'
        if self.hero.monster_name:
            return 'MONSTER'
        # NOTE: we can't filter when hero is moving to_city/from_city
        # or healing in field or fishing
        return 'FIELD'

    @property
    def pet_level(self):
        return self['has_pet'] and self['pet']['pet_level']

    @property
    def pet_is_dead(self):
        return self['has_pet'] in self and self['pet']['is_dead']
