MAKE GODVILLE ZPG AGAIN!

Скрипт написан под Python 3.5, работает без CRON (можно запустить на VPS-ке под tmux-ом, например).
С помощью него был построен храм на 104 день после создания персонажа на штатной пране.
В строительстве ковчега статистика у меня не лучше живой игры, но и не хуже, зато время экономит.

Установка:
git clone https://gitlab.com/vgavro/godville_bot.git
cd ./godville_bot
virtualenv --python=python3 ./env
./env/bin/pip install -r ./requirements.txt

Запуск работы (перед запуском создать конфигурационный файл "hero.yaml"):
./env/bin/python ./hero.py --run

Мониторинг и сбор прано-конденсатора на 7-ми праны:
./env/bin/python /hero.py --gp-cap-monitor=7

Список действий-параметров-условий:
./env/bin/python ./hero.py --help

Пример файла hero.yaml (также см. ./hero_settings.yaml, ./hero.yaml перепределяет его).
На самом деле настроек и действий довольно много, их смотрите в --help и по коду (ну или сделайте мердж-реквест с документацией).

```
auth:
  username: your@mail.com
  password: secret

actions:
  - action: resurrect
  - action: diary_voice_vote
  - action: inventory_activate
    groups: ["acc", "gift", "surprise", "fishing", "invites"]
  - action: voice
    voice: cancel
    quest_regexp: '^стать \d+-м членом гильдии'
    quest_notregexp: '\(отменено\)$'
 
  - action: heal
    voice: true
    encourage: true
    health_percent_lte: 35
    health_gt: 1
    fight_type_in:
      - monster_m
      - monster
      - multi_monster

  - action: heal
    encourage: true
    health_lte: 68
    health_gt: 1
    fight_type_in:
      - dungeon
        #alert: true

  - action: send_to_dungeon
    alert: false
    check_wood: false
    godpower_min: 100
    health_percent_min: 66

  - action: send_to_zpg_arena
    godpower_max: 70
    gold_max: 5000
    quest_progress_lt: 100

params:
  debug: false
# request:
#   proxies:
#     http: 149.56.110.189:8080
#     https: 149.56.110.189:8080
```
